#include <iostream>
#include <vector>

using namespace std;

class Engine
{
    private:
        vector<int> arrVector;
        int         elementToBeSearched;
    
    public:
        Engine(vector<int> aV , int elToBeS)
        {
            arrVector           = aV;
            elementToBeSearched = elToBeS;
        }
    
        void findAndPrintIndexOfElementUsingBinarySearch()
        {
            int  start        = 0;
            int  end          = (int)arrVector.size() - 1;
            bool elementFound = false;
            while(start < end)
            {
                int mid = (start + end) / 2;
                if(elementToBeSearched == arrVector[start])
                {
                    cout<<elementToBeSearched<<" found at index : "<<start<<endl;
                    elementFound = true;
                    break;
                }
                else if(elementToBeSearched == arrVector[mid])
                {
                    cout<<elementToBeSearched<<" found at index : "<<mid<<endl;
                    elementFound = true;
                    break;
                }
                else if(elementToBeSearched == arrVector[end])
                {
                    cout<<elementToBeSearched<<" found at index : "<<end<<endl;
                    elementFound = true;
                    break;
                }
                if(elementToBeSearched > arrVector[mid])
                {
                    start = mid + 1;
                }
                else
                {
                    end = mid - 1;
                }
            }
            if(!elementFound)
            {
                cout<<"Element is not present in array list"<<endl;
            }
        }
};

int main(int argc, const char * argv[])
{
    int         arr[] = {21 , 32 , 43 , 74 , 75 , 86 , 97 , 108 , 144};
    vector<int> arrVector;
    int         len = sizeof(arr)/sizeof(arr[0]);
    for(int i = 0 ; i < len ; i++)
    {
        arrVector.push_back(arr[i]);
    }
    Engine e = Engine(arrVector , 21);
    e.findAndPrintIndexOfElementUsingBinarySearch();
    return 0;
}
